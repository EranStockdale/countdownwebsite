function msToTimeDelta(millisTotal, enableMillis) {
    let milliseconds = millisTotal % 1000; let secondsTotal = Math.floor(millisTotal / 1000);
    let seconds = secondsTotal % 60; let minutesTotal = Math.floor(secondsTotal / 60);
    let minutes = minutesTotal % 60; let hoursTotal = Math.floor(minutesTotal / 60);
    let hours = hoursTotal % 24; let daysTotal = Math.floor(hoursTotal / 24);

    return `${daysTotal} : ${hours} : ${minutes} : ${seconds}` + (enableMillis ? `.${(new String(milliseconds)).padEnd(3, "0")}` : '');
}

function updateTimer(target, { enableMillis }) {
    const desiredDate = target;
    const difference = desiredDate - Date.now();
    $("#countdown-text").text(  difference < 0 ?
                                    'Event Passed!' :
                                    msToTimeDelta(difference, enableMillis) );
}

const urlParams = new URLSearchParams(window.location.search);

const countdownTitle = urlParams.get('title');
const titleObject = $('#title');
titleObject.text(countdownTitle);
$('title').text(countdownTitle);

const enableMillis = urlParams.get('enableMillis') === 'true';
let target = urlParams.get('target');
if (target < 1000000000000) target *= 1000;
setInterval(() => { updateTimer(target, { enableMillis }); }, 1);